@include('layout/header', ['title' => 'Edit Data Buku'])   
    <div class="container pt-4 bg-white"> 

        <h2>Edit Data Buku</h2> 
        @include('flash-message')

        <form action="{{ route('books.update' , $book->id) }}" method="post"> 
            @csrf 
            @method('PUT')

        <div class="mb-3">
            <label class="form-label">Kode Buku</label>
            <input type="text" class="form-control" name="kode_buku" value="{{ $book->kode_buku }}" readonly>
        </div>
        <div class="mb-3">
            <label class="form-label">Judul Buku</label>
            <input type="text" class="form-control" name="judul_buku" value="{{ $book->judul_buku }}">
        </div>
        <div class="mb-3">
            <label class="form-label">Penerbit Buku</label>
            <input type="text" class="form-control" name="penerbit_buku" value="{{ $book->penerbit_buku }}">
        </div>
        <div class="mb-3">
            <label class="form-label">Pengarang Buku</label>
            <input type="text" class="form-control" name="pengarang_buku" value="{{ $book->pengarang_buku }}">
        </div>
        <div class="mb-3">
            <label class="form-label">Jumlah Buku</label>
            <input type="text" class="form-control" name="jumlah_buku" value="{{ $book->jumlah_buku }}">
        </div>
        <td>
        <button type="submit" class="btn btn-primary">Simpan</button>
        <button type="reset" class="btn btn-primary"> Reset </button>
        </td>
        </form>
    </div>

@include('layout/footer')