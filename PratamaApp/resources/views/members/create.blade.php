@include('layout/header', ['title' => 'Form Data Anggota'])  
    <div class="container pt-4 bg-white">
    <h2>Form Pendaftaran Anggota</h2>
    @include('flash-message')
            <form action="{{ route('members.store') }}" method="post"> 
                @csrf
        <div class="mb-3">
            <label class="form-label">Kode Anggota</label>
            <input type="text" class="form-control" name="kode_anggota">
        </div>
        <div class="mb-3">
            <label class="form-label">Nama Lengkap</label>
            <input type="text" class="form-control" name="nama_anggota">
        </div>
        <div class="mb-3">
            <label class="form-label">No KTP</label>
            <input type="number" class="form-control" name="KTP">
        </div>
        <div class="mb-3">
            <label class="form-label">Email</label>
            <input type="email" class="form-control" name="Email">
        </div>
        <div class="mb-3">
            <label class="form-label">Nomor Telepon</label>
            <input type="number" class="form-control" name="nomor_telepon">
        </div>
        <div class="mb-3">
            <label class="form-label">Tanggal Daftar</label>
            <input type="date" class="form-control" name="tanggal_daftar">
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
    </div>
    @include('layout/footer')