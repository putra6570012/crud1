<?php

namespace App\Http\Controllers;

use App\Models\book;
use Illuminate\Http\Request;

class bookController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $books = book::all();
        return view('books.index', ['books' => $books]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //validasi
        $validated = $request->validate([
            'kode_buku' => 'required',
            'judul_buku' => 'required',
            'penerbit_buku' => 'required',
            'pengarang_buku' => 'required',
            'jumlah_buku' => 'required|integer|min:1',
        ]);

        //simpan ke database
        $book = book::create([
            'kode_buku' => $request->input ('kode_buku'),
            'judul_buku' => $request->input ('judul_buku'),
            'penerbit_buku' => $request->input ('penerbit_buku'),
            'pengarang_buku' => $request->input ('pengarang_buku'),
            'jumlah_buku' => $request->input ('jumlah_buku'),


        ]);

        //flash message
        if($book){
            return redirect()->route('books.index')->with('success','Buku Berhasil Disimpan');
        }
        else{
            return redirect()->back()->with('error','Buku Gagal Disimpan');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(book $book)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $book = book::find($id);

        return view('books.edit', ['book' => $book]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Book $book){
        $validated = $request->validate([
            'kode_buku' => 'required',
            'judul_buku' => 'required',
            'penerbit_buku' => 'required',
            'pengarang_buku' => 'required',
            'jumlah_buku' => 'required|integer|min:0',
        ]);

        $book->update([
            'kode_buku' => $request->input('kode_buku'),
            'judul_buku' => $request->input('judul_buku'),
            'penerbit_buku' => $request->input('penerbit_buku'),
            'pengarang_buku' => $request->input('pengarang_buku'),
            'jumlah_buku' => $request->input('jumlah_buku'),
        ]);

        if ($book) {
            return redirect()->route('books.index')->with('success', 'Data Berhasil Diperbarui');
        } else {
            return redirect()->back()->with('error', 'Data Gagal Diperbarui');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
    
        $books = book::destroy($id);
        $books = book::all();   
        return view('books.index', ['books' => $books]);
    }

    
}
