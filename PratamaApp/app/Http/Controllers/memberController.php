<?php

namespace App\Http\Controllers;

use App\Models\member;
use Illuminate\Http\Request;

class memberController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $members = member::all();
       return  view('members.index', ['members' => $members]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('members.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //validasi
        $validated = $request->validate([
            'kode_anggota' => 'required',
            'nama_anggota' => 'required',
            'KTP' => 'required',
            'Email' => 'required',
            'nomor_telepon' => 'required',
            'tanggal_daftar' => 'required',
        ]);

        //simpan ke database
        $member = Member::create([
            'kode_anggota' => $request->input ('kode_anggota'),
            'nama_anggota' => $request->input ('nama_anggota'),
            'KTP' => $request->input ('KTP'),
            'Email' => $request->input ('Email'),
            'nomor_telepon' => $request->input ('nomor_telepon'),
            'tanggal_daftar' => $request->input ('tanggal_daftar'),
        ]);
        
        if($member){
            return redirect()->route('members.index')->with('success','Buku Berhasil Disimpan');
        }
        else{
            return redirect()->back()->with('error','Buku Gagal Disimpan');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(member $member)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $member = member::find($id);

        return view('members.edit', ['member' => $member]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, member $member)
    {
        $validated = $request->validate([
            'kode_anggota' => 'required',
            'nama_anggota' => 'required',
            'KTP' => 'required',
            'Email' => 'required',
            'nomor_telepon' => 'required',
            'tanggal_daftar' => 'required',
        ]);

        $member->update([
            'kode_anggota' => $request->input('kode_anggota'),
            'nama_anggota' => $request->input('nama_anggota'),
            'KTP' => $request->input('KTP'),
            'Email' => $request->input('Email'),
            'nomor_telepon' => $request->input('nomor_telepon'),
            'tanggal_daftar' => $request->input('tanggal_daftar'),
        ]);

        if ($member) {
            return redirect()->route('members.index')->with('success', 'Data Anggota Berhasil Diperbarui');
        } else {
            return redirect()->back()->with('error', 'Data Anggota Gagal Diperbarui');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $member = member::destroy($id);
        $members = member::all();   
        return view('members.index', ['members' => $members]);
    }
}
