<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class member extends Model
{
    use HasFactory;
    protected $fillable = ['kode_anggota','nama_anggota', 'KTP','Email','nomor_telepon','tanggal_daftar'];
}
